#!/bin/bash

set -ueo pipefail
cd "$(dirname "${BASH_SOURCE[0]}")"

cd app

meteor npm install

MONGO_URL=mongodb://localhost/p \
MONGO_OPLOG_URL=mongodb://localhost/local \
meteor run \
--port 4831 \
