module.exports = {
    root: true,
    parser: "babel-eslint",
    parserOptions: {
        ecmaVersion: 6,
        sourceType: "module",
        ecmaFeatures: {
            jsx: true,
        },
    },
    plugins: ["meteor"],
    extends: [
        "airbnb/base",
        "plugin:meteor/recommended",
        "plugin:react/recommended",
        "prettier",
    ],
    env: {
        meteor: true,
        browser: true,
        node: true,
    },
    settings: {
        "import/resolver": "meteor",
        "import/core-modules": ["meteor/session"],
        react: {
            version: "16.4.2",
        },
    },
    rules: {
        "import/prefer-default-export": ["off"],
        "import/extensions": ["off"],
        "import/no-absolute-path": ["error", { esmodule: false }],
        "max-len": [
            "warn",
            { code: 120, ignoreStrings: true, ignoreComments: true },
        ],
        "no-underscore-dangle": ["error", { allow: ["_id"] }],
        "import/no-extraneous-dependencies": ["off"],
        "import/first": ["off"],
        "one-var": ["error", "never"],
        "no-console": ["warn", { allow: ["error"] }],
        "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],
        "prefer-destructuring": ["off"],
    },
};
