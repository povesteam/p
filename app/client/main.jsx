import "moment/locale/ro";
import React from "react";
import App from "/imports/ui/App";
import { render } from "react-dom";
import { Router } from "react-router";
import { Meteor } from "meteor/meteor";
import { createBrowserHistory } from "history";

import "/imports/api/i18n";
import "/imports/accounts/client.js";

const history = createBrowserHistory();

Meteor.startup(() => {
    render(
        <Router history={history}>
            <App />
        </Router>,
        document.getElementById("react-target"),
    );
});
