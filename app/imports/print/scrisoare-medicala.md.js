export default `
Denumire furnizor:

# Scrisoare medicală

Stimat(ă) coleg(ă), vă informăm că pacientul(a) **{patient.firstName} {patient.lastName}**
născut la data de {patient.birthDate}
CNP/cod unic de asigurare {patient.idNumber}
a fost consultat în serviciul nostru la data de {investigation.date}
nr. F.O./nr. din registrul de consultații.

## Motivele prezentării

> {investigation.symptoms}

## Diagnosticul

> {investigation.diagnosis}

## Anamneza

> ??

## Factori de risc

> ??

## Examen clinic

> {investigation.physicalExamination}

## Examene de laborator

> ??

## Examene paraclinice

> ??

## Tratament efectuat

> ??

## Alte informații referitoare la starea de sănătate a asiguratului

> {investigation.otherInfo}

## Tratament recomandat

> {investigation.recommendations}

### Se completează obligatoriu una din cele două informații:
- {prescription.opt1} S-a eliberat prescripție medicală, caz în care se va înscrie seria şi numărul acesteia
- {prescription.opt2} Nu s-a eliberat prescripție medicală deoarece nu a fost necesar
- {prescription.opt3} Nu s-a eliberat prescripție medicală

### Se completează obligatoriu una din cele două informații:
- {leave.opt1} S-a eliberat concediu medical la externare, caz în care se va înscrie seria şi numărul acestuia {leave.medicalLeaveNumber}
- {leave.opt2} Nu s-a eliberat concediu medical la externare deoarece nu a fost necesar
- {leave.opt3} Nu s-a eliberat concediu medical la externare

### Se completează obligatoriu una din cele două informații:
- {home.opt1} S-a eliberat recomandare pentru îngrijiri medicale la domiciliu/paliative la domiciliu
- {home.opt2} Nu s-a eliberat recomandare pentru îndrijiri medicale la domiciliu/paliative la domiciliu, deoarece nu a fost necesar

### Se completează obligatoriu una din cele două informații:
- {devices.opt1} S-a eliberat prescripție medicală pentru dispozitive medicale în ambulatoriu
- {devices.opt2} Nu s-a eliberat prescripție medicală pentru dispozitive medicale în ambulatoriu deoarece nu a fost necesar

----

|         Data         | Semnătura și parafa medicului |
| :------------------: | :---------------------------: |
| {investigation.date} |       {medic.fullName}        |
|                      |     {medic.specialitate}      |
|                      |       {medic.codParafa}       |

----

## Calea de transmitere
- prin asigurat
- prin poştă

*) Scrisoarea medicală se întocmeşte în două exemplare, din care un exemplar rămâne la medicul
care a efectuat consultaţia/serviciul în ambulatoriul de specialitate, iar un exemplar este transmis
medicului de familie/medicului de specialitate din ambulatoriul de specialitate.
Scrisoarea medicală sau biletul de ieşire din spital sunt documente tipizate care se întocmesc la
data externării, într-un singur exemplar care este transmis medicului de familie/medicului de
specialitate din ambulatoriul de specialitate, direct ori prin intermediul asiguratului.

`;
