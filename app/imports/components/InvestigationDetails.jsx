import moment from "moment";
import map from "lodash/map";
import get from "lodash/get";
import Loader from "./Loader";
import Form from "./FormWithWidgets";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import subscribe from "/imports/api/subscribe";
import React, { useState, useEffect } from "react";
import { withTracker } from "meteor/react-meteor-data";
import Investigations from "/imports/models/investigations";
import { Heading, Pane, majorScale, toaster, Button } from "evergreen-ui";

const InvestigationDetails = withTracker(({ patientId, investigationId }) => {
    const medics = Meteor.users
        .find({ roles: "medic" }, { sort: { "profile.name": 1 } })
        .fetch();

    let investigation;
    let loading;

    if (investigationId === "new") {
        investigation = {
            patientId,
        };
    } else {
        const handles = [subscribe("investigation", { investigationId })];
        loading = handles.some(handle => !handle.ready());
        investigation = Investigations.findOne({ _id: investigationId });

        if (get(investigation, "date")) {
            investigation.date = moment(investigation.date)
                .millisecond(0)
                .second(0)
                .toISOString();
        }
    }

    return {
        loading,
        investigation,
        medics,
    };
})(({ patientId, loading, investigation, medics }) => {
    const { t } = useTranslation();
    const history = useHistory();
    const [saving, setSaving] = useState(false);
    const [formData, setFormData] = useState();

    useEffect(() => {
        if (investigation) {
            setFormData(investigation);
        }
    }, [investigation]);

    if (loading) {
        return <Loader />;
    }

    const schema = {
        type: "object",
        properties: {
            medicId: {
                title: t("Medic"),
                type: "string",
                enum: map(medics, "_id"),
                enumNames: map(medics, "profile.name"),
            },
            date: {
                title: t("Date"),
                type: "string",
                format: "date-time",
            },
            symptoms: {
                title: t("Motivele prezentarii"),
                type: "string",
            },
            medicalHistory: {
                title: t("Medical history"),
                type: "string",
            },
            allergies: {
                title: t("Allergies"),
                type: "string",
            },
            physicalExamination: {
                title: t("Physical examination"),
                type: "string",
            },
            diagnosis: {
                title: t("Diagnosis"),
                type: "string",
            },
            recommendations: {
                title: t("Recommendations"),
                type: "string",
            },
            medicalLeave: {
                title: t("Medical leave"),
                type: "boolean",
            },
            medicalLeaveNumber: {
                title: t("Medical leave number"),
                type: ["string", "null"],
            },
            otherInfo: {
                title: t("Other information"),
                type: "string",
            },
        },
    };

    const uiSchema = {
        symptoms: {
            "ui:widget": "textarea",
        },
        medicalHistory: {
            "ui:widget": "textarea",
        },
        allergies: {
            "ui:widget": "textarea",
        },
        physicalExamination: {
            "ui:widget": "textarea",
        },
        diagnosis: {
            "ui:widget": "textarea",
        },
        recommendations: {
            "ui:widget": "textarea",
        },
        medicalLeaveNumber: {
            "ui:emptyValue": null,
        },
        otherInfo: {
            "ui:widget": "textarea",
        },
    };

    const submitButton = (
        <Pane
            background="tint2"
            display="flex"
            position="sticky"
            bottom="0"
            borderTop
            paddingTop={majorScale(2)}
            paddingBottom={majorScale(2)}
        >
            <Pane>
                <Button
                    type="submit"
                    appearance="primary"
                    marginLeft={majorScale(2)}
                    iconBefore={saving ? null : "floppy-disk"}
                    isLoading={saving}
                >
                    {t("Save")}
                </Button>

                <Button
                    marginLeft={majorScale(2)}
                    iconBefore={"print"}
                    disabled={saving}
                    onClick={() =>
                        history.push(
                            `/print/investigation/${investigation.patientId}/${investigation._id}`,
                        )
                    }
                >
                    Print
                </Button>
            </Pane>
        </Pane>
    );

    return (
        <Pane padding={majorScale(2)}>
            <Heading>{t("Investigation details")}</Heading>

            <Form
                schema={schema}
                uiSchema={uiSchema}
                formData={formData}
                onSubmit={({ formData }) => {
                    setSaving(true);
                    setFormData(formData);

                    Meteor.call(
                        "saveInvestigation",
                        {
                            doc: formData,
                        },
                        (err, res) => {
                            setSaving(false);

                            if (err) {
                                console.error(
                                    "Error while saving investigation",
                                    err,
                                );

                                toaster.danger(
                                    t("Cannot save investigation data"),
                                    {
                                        id: "saveInvestigation",
                                        description: err.reason || t(err.error),
                                    },
                                );

                                return;
                            }

                            history.push(
                                `/list/investigation/${patientId}/${res._id}`,
                            );

                            toaster.success(t("Investigation data saved"), {
                                id: "saveInvestigation",
                            });
                        },
                    );
                }}
                onError={form =>
                    console.error("Cannot save investigation", form)
                }
            >
                {submitButton}
            </Form>
        </Pane>
    );
});

export default InvestigationDetails;
