import map from "lodash/map";
import filter from "lodash/filter";
import styled from "styled-components";
import React, { Fragment } from "react";

const Separator = styled.span`
    opacity: 0.3;
    margin: 0 0.5em;
`;

const NoWrap = styled.span`
    white-space: nowrap;
`;

const Join = ({ children }) => (
    <>
        {map(filter(children), (child, index) => (
            <Fragment key={index}>
                {index > 0 && <Separator>&middot;&#8203;</Separator>}
                <NoWrap>{child}</NoWrap>
            </Fragment>
        ))}
    </>
);

export default Join;
