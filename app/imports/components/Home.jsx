// inspired from https://github.com/jquense/react-big-calendar/blob/master/examples/demos/basic.js

import moment from "moment";
import map from "lodash/map";
import get from "lodash/get";
import Loader from "./Loader";
import keyBy from "lodash/keyBy";
import filter from "lodash/filter";
import UserAvatar from "./UserAvatar";
import styled from "styled-components";
import includes from "lodash/includes";
import React, { useState } from "react";
import BigCalendar from "react-big-calendar";
import { useHistory } from "react-router-dom";
import subscribe from "/imports/api/subscribe";
import { useTranslation } from "react-i18next";
import Patients from "/imports/models/patients";
import { PatientHeaderSmall } from "./PatientHeader";
import { withTracker } from "meteor/react-meteor-data";
import { Button, SelectMenu, Text } from "evergreen-ui";
import Investigations from "/imports/models/investigations";
import bigCalendarCSS from "react-big-calendar/lib/css/react-big-calendar.css";

const BigCalendarStyle = styled.div`
    height: 100vh;

    ${bigCalendarCSS}
`;

const localizer = BigCalendar.momentLocalizer(moment);

let Basic = ({ loading, events, medics, patients }) => {
    const { t } = useTranslation();
    const history = useHistory();
    const [selectedMedicId, setSelectedMedicId] = useState(
        includes(get(Meteor.user(), "roles"), "medic") ? Meteor.userId() : "*",
    );

    if (loading) {
        return <Loader />;
    }

    return (
        <>
            <SelectMenu
                hasTitle={false}
                options={[
                    { label: t("All medics"), value: "*" },
                    ...map(medics, medic => ({
                        label: medic.profile.name,
                        value: medic._id,
                    })),
                ]}
                selected={selectedMedicId}
                onSelect={item => setSelectedMedicId(item.value)}
                closeOnSelect={true}
            >
                <Button>
                    {selectedMedicId === "*" ? (
                        <Text>{t("All medics")}</Text>
                    ) : (
                        <UserAvatar user={medics[selectedMedicId]} withName />
                    )}
                </Button>
            </SelectMenu>

            <BigCalendarStyle>
                <BigCalendar.Calendar
                    localizer={localizer}
                    events={filter(
                        events,
                        ({ investigation }) =>
                            selectedMedicId === "*" ||
                            selectedMedicId === investigation.medicId,
                    )}
                    views={["month", "week", "work_week", "day", "agenda"]}
                    defaultView="week"
                    step={60}
                    popup={true}
                    onSelectEvent={({ investigation }) => {
                        history.push(
                            `/list/investigation/${investigation.patientId}/${investigation._id}`,
                        );
                    }}
                    titleAccessor={({ investigation }) => {
                        const patient = patients[investigation.patientId];

                        return <PatientHeaderSmall patient={patient} />;
                    }}
                />
            </BigCalendarStyle>
        </>
    );
};

const calendarData = withTracker(() => {
    const handles = [
        subscribe("investigationsForReceptionist"),
        subscribe("patientsForReceptionist"),
        subscribe("medics"),
    ];

    const investigations = Investigations.find().fetch();

    const events = map(investigations, investigation => ({
        start: investigation.date,
        end: moment(investigation.date)
            .add(1, "hour")
            .toDate(),
        investigation,
    }));

    const medics = keyBy(Meteor.users.find({ roles: "medic" }).fetch(), "_id");

    const patients = keyBy(Patients.find().fetch(), "_id");

    return {
        loading: handles.some(handle => !handle.ready()),
        events,
        medics,
        patients,
    };
});

export default calendarData(Basic);
