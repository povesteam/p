// inspired from https://github.com/segmentio/evergreen/blob/master/docs/src/components/examples/AdvancedTable.js

import Fuse from "fuse.js";
import map from "lodash/map";
import Loader from "./Loader";
import React, { useState } from "react";
import PatientAvatar from "./PatientAvatar";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { Table, Popover, Position, Menu, Text, IconButton } from "evergreen-ui";

const RowMenu = ({ profile }) => {
    const { t } = useTranslation();

    const history = useHistory();

    return (
        <Menu>
            <Menu.Group>
                <Menu.Item
                    icon="edit"
                    secondaryText="⌘E"
                    onSelect={() =>
                        history.push(`/edit/patient/${profile._id}`)
                    }
                >
                    {t("Edit")}
                </Menu.Item>
            </Menu.Group>
            <Menu.Divider />
            <Menu.Group>
                <Menu.Item icon="trash" intent="danger">
                    {t("Delete")}
                </Menu.Item>
            </Menu.Group>
        </Menu>
    );
};

const Row = ({ profile }) => {
    const history = useHistory();

    return (
        <Table.Row
            key={profile._id}
            isSelectable
            onSelect={() => {
                history.push(`/view/patient/${profile._id}`);
            }}
        >
            <Table.Cell display="flex" alignItems="center">
                <PatientAvatar patient={profile} />
                <Text marginLeft={8} size={300} fontWeight={500}>
                    {profile.firstName} {profile.lastName}
                </Text>
            </Table.Cell>
            <Table.TextCell>{profile.gender}</Table.TextCell>
            <Table.TextCell>{profile.idNumber}</Table.TextCell>
            {/* <Table.TextCell>LAST CONSULT TODO</Table.TextCell> */}
            <Table.Cell width={48} flex="none">
                <Popover
                    content={<RowMenu profile={profile} />}
                    position={Position.BOTTOM_RIGHT}
                >
                    <IconButton icon="more" height={24} appearance="minimal" />
                </Popover>
            </Table.Cell>
        </Table.Row>
    );
};

const AdvancedTable = ({ loading, patients }) => {
    const { t } = useTranslation();

    const [query, setQuery] = useState("");

    if (loading) {
        return <Loader />;
    }

    const queryTrim = query.trim();

    let items;

    if (queryTrim) {
        items = new Fuse(patients, {
            keys: ["firstName", "lastName", "idNumber"],
        }).search(queryTrim);
    } else {
        items = patients;
    }

    return (
        <Table border>
            <Table.Head>
                <Table.SearchHeaderCell
                    onChange={setQuery}
                    value={query}
                    placeholder={t("Search")}
                />
                <Table.HeaderCell width={48} flex="none" />
            </Table.Head>
            <Table.VirtualBody height={640}>
                {map(items, item => (
                    <Row key={item._id} profile={item} />
                ))}
            </Table.VirtualBody>
        </Table>
    );
};

export default AdvancedTable;
