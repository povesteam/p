import React from "react";
import moment from "moment";
import map from "lodash/map";
import get from "lodash/get";
import Loader from "./Loader";
import PatientHeader from "./PatientHeader";
import { useTranslation } from "react-i18next";
import subscribe from "/imports/api/subscribe";
import { withTracker } from "meteor/react-meteor-data";
import { useParams, useHistory } from "react-router-dom";
import InvestigationDetails from "./InvestigationDetails";
import Investigations from "/imports/models/investigations";
import {
    Pane,
    Button,
    majorScale,
    Tooltip,
    Position,
    Text,
    Table,
} from "evergreen-ui";

const InvestigationHeader = ({ investigation }) => {
    const now = moment();
    const investigationMoment = moment(investigation.date);

    const opacity = investigationMoment.isSame(now, "day") ? 1 : 0.3;

    return (
        <Pane display="flex" opacity={opacity}>
            <Pane flex={1}>
                <Text>{investigation.medicName || <em>--</em>}</Text>
            </Pane>
            <Pane>
                <Text>{investigationMoment.calendar()}</Text>
            </Pane>
        </Pane>
    );
};

const InvestigationHeaderList = withTracker(({ patientId }) => {
    const handles = [
        subscribe("investigations", { patientId }),
        subscribe("medics", {}),
    ];

    return {
        loading: handles.some(handle => !handle.ready()),
        investigations: Investigations.find(
            { patientId },
            {
                sort: { date: -1 },
                transform: c => ({
                    ...c,
                    medicName: get(
                        Meteor.users.findOne({ _id: c.medicId }),
                        "profile.name",
                    ),
                }),
            },
        ).fetch(),
    };
})(({ loading, patientId, investigations }) => {
    const { t } = useTranslation();
    const history = useHistory();

    const { investigationId } = useParams();

    if (loading) {
        return <Loader />;
    }

    return (
        <>
            <Button
                iconBefore="plus"
                onClick={() => {
                    history.push(`/list/investigation/${patientId}/new`);
                }}
            >
                {t("New investigation")}
            </Button>

            <Table>
                <Table.Body>
                    {map(investigations, investigation => (
                        <Table.Row
                            key={investigation._id}
                            isSelected={investigation._id === investigationId}
                            isSelectable
                            onSelect={() =>
                                history.replace(
                                    `/list/investigation/${patientId}/${investigation._id}`,
                                )
                            }
                        >
                            <Tooltip
                                content={moment(investigation.date).format(
                                    "llll",
                                )}
                                position={Position.RIGHT}
                            >
                                <Table.TextCell>
                                    <InvestigationHeader
                                        investigation={investigation}
                                    />
                                </Table.TextCell>
                            </Tooltip>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        </>
    );
});

const InvestigationPage = () => {
    const { patientId, investigationId } = useParams();

    return (
        <>
            <Pane marginBottom={majorScale(2)}>
                <PatientHeader patientId={patientId} />
            </Pane>

            <Pane display="flex">
                <Pane width={240} borderRight>
                    <InvestigationHeaderList patientId={patientId} />
                </Pane>
                {investigationId && (
                    <Pane flex={1}>
                        <InvestigationDetails
                            patientId={patientId}
                            investigationId={investigationId}
                        />
                    </Pane>
                )}
            </Pane>
        </>
    );
};

export default InvestigationPage;
