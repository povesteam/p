import { userHasRole } from "/imports/api/roles";
import { withTracker } from "meteor/react-meteor-data";

const Gate = withTracker(({ role }) => ({
    hasRole: userHasRole(Meteor.user(), role),
}))(({ hasRole, children }) => (hasRole ? children : null));

export default Gate;
