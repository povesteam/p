import moment from "moment";
import get from "lodash/get";
import compose from "lodash/fp/compose";
import React, { useEffect } from "react";
import ReactMarkdown from "react-markdown";
import subscribe from "/imports/api/subscribe";
import Patients from "/imports/models/patients";
import { withTracker } from "meteor/react-meteor-data";
import Investigations from "/imports/models/investigations";
import styled, { createGlobalStyle } from "styled-components";
import withRouterParams from "/imports/hocs/withRouterParams";
import scrisoareMedicala from "/imports/print/scrisoare-medicala.md.js";

export const GlobalStyle = createGlobalStyle`
    /*
    @media print {
        @page {
            margin-top: 0;
            margin-bottom: 0;
            margin-left: 2cm;
            margin-right: 2cm;
        }
    }
    */
`;

const PageWrapper = styled.div`
    @media screen {
        max-width: 800px;
        min-height: 1100px;
        margin: 20px auto;
        padding: 50px;
        border: 1px solid rgba(0, 0, 0, 0.5);
        box-shadow: 2px 3px 20px 0px rgba(0, 0, 0, 0.3);
    }
`;

const MarkdownStyle = styled.div`
    font-family: sans-serif;
    line-height: 1.5em;

    h1 {
        margin: 0.75em 0 0 0;
        font-weight: bold;
        font-size: 2em;
        text-align: center;
    }

    h2 {
        margin: 0.75em 0 0 0;
        font-weight: bold;
        font-size: 1.2em;
    }

    h3 {
        margin: 0.75em 0 0 0;
        font-weight: bold;
    }

    p {
        margin: 0.75em 0 0 0;
        text-indent: 2em;
    }

    blockquote {
        white-space: pre-wrap;

        p {
            text-indent: initial;
            margin-left: 2em;
        }
    }

    table {
        width: 100%;
    }

    th {
        font-weight: bold;
    }

    td {
        vertical-align: middle;
    }

    li {
        img {
            vertical-align: text-bottom;
        }
    }

    em {
        font-style: italic;
    }

    strong {
        font-weight: bold;
    }
`;

const checkIcon = checked =>
    checked
        ? "![checked](/icons/check-square.svg)"
        : "![unchecked](/icons/square.svg)";

const InvestigationPrintPage = compose([
    withRouterParams(),
    withTracker(({ patientId, investigationId }) => {
        const handles = [
            subscribe("patient", { patientId }),
            subscribe("investigation", { investigationId }),
        ];

        return {
            loading: handles.some(handle => !handle.ready()),
            patient: Patients.findOne({ _id: patientId }),
            investigation: Investigations.findOne({ _id: investigationId }),
        };
    }),
])(({ loading, patient, investigation }) => {
    if (loading) {
        return <div>Loading</div>;
    }

    const medic = {}; //todo

    const markdownKeys = {
        patient: {
            ...patient,
            birthDate: moment(patient.birthDate).format("LL"),
        },
        investigation: {
            ...investigation,
            date: moment(investigation.date || moment.invalid).format("LL"),
        },
        medic,
    };

    const templateKeys = {
        prescription: {
            opt1: checkIcon(true),
            opt2: checkIcon(false),
            opt3: checkIcon(false),
        },
        leave: {
            medicalLeaveNumber: investigation.medicalLeave
                ? `**${investigation.medicalLeaveNumber || "--"}**`
                : null,
            opt1: checkIcon(investigation.medicalLeave),
            opt2: checkIcon(!investigation.medicalLeave),
            opt3: checkIcon(false),
        },
        home: {
            opt1: checkIcon(false),
            opt2: checkIcon(true),
        },
        devices: {
            opt1: checkIcon(false),
            opt2: checkIcon(true),
        },
    };

    const textRenderer = ({ value }) => {
        return value.replace(
            /{([^{}]+)}/g,
            (match, keyPath) => get(markdownKeys, keyPath) || "--",
        );
    };

    const markdown = scrisoareMedicala.replace(
        /{((prescription|leave|home|devices)\.[^{}]+)}/g,
        (match, keyPath) => get(templateKeys, keyPath) || "",
    );

    useEffect(() => {
        const prevTitle = document.title;

        document.title = `${get(patient, "firstName") || ""} ${get(
            patient,
            "lastName",
        ) || ""}`;

        return () => (document.title = prevTitle);
    }, [patient]);

    const goBack = () => {
        history.back();
    };

    useEffect(() => {
        window.addEventListener("afterprint", goBack);

        window.print();

        return () => window.removeEventListener("afterprint", goBack);
    }, []);

    return (
        <>
            <GlobalStyle />

            <PageWrapper>
                <table>
                    <thead>
                        <tr>
                            <th>
                                <img src="/medic-logo.png" />
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <MarkdownStyle>
                                    <ReactMarkdown
                                        source={markdown}
                                        renderers={{
                                            text: textRenderer,
                                        }}
                                    />
                                </MarkdownStyle>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </PageWrapper>
        </>
    );
});

export default InvestigationPrintPage;
