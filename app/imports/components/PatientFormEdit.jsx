import React from "react";
import PatientForm from "./PatientForm";
import compose from "lodash/fp/compose";
import subscribe from "/imports/api/subscribe";
import Patients from "/imports/models/patients";
import { withTracker } from "meteor/react-meteor-data";
import withRouterParams from "/imports/hocs/withRouterParams";

const PatientFormEdit = compose([
    withRouterParams(),
    withTracker(({ patientId }) => {
        const handle = subscribe("patient", { patientId });

        return {
            loading: !handle.ready(),
            patient: Patients.findOne({ _id: patientId }),
        };
    }),
])(({ patient, ...props }) => {
    return <PatientForm data={patient} {...props} />;
});

export default PatientFormEdit;
