import React from "react";
import Join from "./Join";
import moment from "moment";
import get from "lodash/get";
import first from "lodash/first";
import PatientAvatar from "./PatientAvatar";
import subscribe from "/imports/api/subscribe";
import Patients from "/imports/models/patients";
import { withTracker } from "meteor/react-meteor-data";
import { Pane, majorScale, Heading, Paragraph } from "evergreen-ui";

export const PatientHeaderSmall = ({ patient }) => {
    return (
        <>
            <PatientAvatar patient={patient} size={majorScale(2)} />
            &nbsp;
            {first(get(patient, "firstName"))}. {get(patient, "lastName")}
        </>
    );
};

export const PatientHeaderWidget = ({ patient }) => {
    let patientAge;

    if (get(patient, "birthDate")) {
        patientAge = moment()
            .subtract(moment().diff(patient.birthDate, "y"), "years")
            .fromNow(true);
    }

    return (
        <Pane display="flex">
            <Pane>
                <PatientAvatar
                    patient={patient}
                    size={majorScale(4)}
                    marginRight={majorScale(2)}
                />
            </Pane>
            <Pane flex={1}>
                <Heading size={700}>
                    {get(patient, "firstName")} {get(patient, "lastName")}
                </Heading>
                <Paragraph>
                    <Join>
                        {get(patient, "birthDate")}
                        {patientAge}
                        {get(patient, "gender")}
                        {get(patient, "idNumber")}
                        {get(patient, "idDocument")}
                        {get(patient, "countryCode")}
                        {get(patient, "region")}
                        {get(patient, "city")}
                        {get(patient, "address")}
                        {get(patient, "postCode")}
                        {get(patient, "phone")}
                        {get(patient, "email")}
                    </Join>
                </Paragraph>
            </Pane>
        </Pane>
    );
};

export default withTracker(({ patientId }) => {
    const handle = subscribe("patient", { patientId });

    return {
        loading: !handle.ready(),
        patient: Patients.findOne({ _id: patientId }),
    };
})(PatientHeaderWidget);
