// inspired from https://github.com/segmentio/evergreen/blob/master/docs/src/components/examples/AdvancedTable.js

import React from "react";
import Join from "./Join";
import map from "lodash/map";
import get from "lodash/get";
import Loader from "./Loader";
import UserAvatar from "./UserAvatar";
import { useHistory } from "react-router-dom";
import { Table, Text, Pane, majorScale } from "evergreen-ui";

const Row = ({ user }) => {
    const history = useHistory();

    return (
        <Table.Row
            key={user._id}
            isSelectable
            onSelect={() => {
                history.push(`/edit/user/${user._id}`);
            }}
        >
            <Table.Cell display="flex" alignItems="center">
                <UserAvatar user={user} withName />
            </Table.Cell>
            <Table.TextCell>{get(user, "username")}</Table.TextCell>
            <Table.TextCell>
                <Join children={get(user, "roles")} />
            </Table.TextCell>
        </Table.Row>
    );
};

const AdvancedTable = ({ loading, users }) => (
    <Pane padding={16} borderRadius={3} maxWidth={500} marginX="auto">
        {loading ? (
            <Loader />
        ) : (
            <Table border>
                <Table.VirtualBody height={640}>
                    {map(users, user => (
                        <Row key={user._id} user={user} />
                    ))}
                </Table.VirtualBody>
            </Table>
        )}
    </Pane>
);

export default AdvancedTable;
