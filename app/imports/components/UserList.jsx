import subscribe from "/imports/api/subscribe";
import UserListTable from "./UserListTable.jsx";
import { withTracker } from "meteor/react-meteor-data";

export default withTracker(() => {
    const usersHandle = subscribe("users");

    const loading = !usersHandle.ready();

    const users = Meteor.users
        .find(
            {},
            {
                sort: {
                    createdAt: -1,
                },
            },
        )
        .fetch();

    return {
        loading,
        users,
    };
})(UserListTable);
