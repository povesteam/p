import get from "lodash/get";
import Loader from "./Loader";
import Form from "./FormWithWidgets";
import UserAvatar from "./UserAvatar";
import includes from "lodash/includes";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import React, { useState, useEffect } from "react";
import { Heading, Button, Pane, majorScale, toaster } from "evergreen-ui";

// https://react-jsonschema-form.readthedocs.io/en/latest/form-customization/

const UserForm = ({ loading, user }) => {
    const { t } = useTranslation();
    const history = useHistory();

    const [saving, setSaving] = useState(false);
    const [formData, setFormData] = useState();

    useEffect(() => {
        if (user) {
            setFormData(user);
        }
    }, [user]);

    if (loading || !formData) {
        return <Loader />;
    }

    const medicFields = {};

    if (includes(get(formData, "roles"), "medic")) {
        medicFields.investigationTypes = {
            type: "array",
            title: t("Investigation types"),
            items: {
                type: "string",
                enum: ["checkup", "eeg", "emg"],
                enumNames: [t("Checkup"), t("EEG"), t("EMG")],
            },
            default: ["checkup"],
            uniqueItems: true,
        };
    }

    const schema = {
        type: "object",
        required: ["username", "roles"],
        properties: {
            username: {
                type: "string",
                title: "Username",
            },
            password: {
                type: "string",
                title: t("Password"),
            },
            profile: {
                type: "object",
                title: "",
                properties: {
                    firstName: {
                        type: "string",
                        title: t("First name"),
                    },
                    lastName: {
                        type: "string",
                        title: t("Last name"),
                    },
                },
            },
            roles: {
                type: "array",
                title: t("Role"),
                items: {
                    type: "string",
                    enum: ["receptionist", "medic", "admin"],
                    enumNames: [
                        t("Receptionist"),
                        t("Medic"),
                        t("Administrator"),
                    ],
                    enumDisabled: "admin",
                },
                uniqueItems: true,
            },
            ...medicFields,
        },
    };

    const uiSchema = {
        password: {
            "ui:widget": "password",
            "ui:help": t("Leave empty to keep current password"),
        },
        roles: {
            "ui:widget": "checkboxes",
            "ui:options": {
                enumDisabled:
                    Meteor.userId() === get(user, "_id") ? ["admin"] : [],
            },
        },
        investigationTypes: {
            "ui:widget": "checkboxes",
        },
    };

    const headings = user ? (
        <Pane paddingBottom={majorScale(2)}>
            <UserAvatar user={user} withName big />
        </Pane>
    ) : (
        <Heading size={700} paddingBottom={majorScale(2)}>
            {t("New user")}
        </Heading>
    );

    const submitButton = (
        <Pane
            background="tint2"
            display="flex"
            flexDirection="row-reverse"
            position="sticky"
            bottom="0"
            borderTop
            paddingTop={majorScale(2)}
            paddingBottom={majorScale(2)}
        >
            <Pane>
                <Button
                    type="submit"
                    appearance="primary"
                    iconBefore={saving ? null : "floppy-disk"}
                    isLoading={saving}
                >
                    {t("Save")}
                </Button>
            </Pane>
            <Pane flex={1}>
                <Button
                    appearance="minimal"
                    iconBefore="arrow-left"
                    onClick={() => history.push("/list/patient")}
                >
                    {t("Back")}
                </Button>
            </Pane>
        </Pane>
    );

    return (
        <>
            {headings}

            <Form
                disabled={saving}
                schema={schema}
                uiSchema={uiSchema}
                formData={formData}
                onChange={({ formData }) => {
                    setFormData(formData);
                }}
                onSubmit={({ formData }) => {
                    setSaving(true);
                    setFormData(formData);

                    Meteor.call(
                        "saveUser",
                        {
                            doc: formData,
                        },
                        (err, res) => {
                            setSaving(false);

                            if (err) {
                                console.error("Error while saving user", err);

                                toaster.danger(t("Cannot save user data"), {
                                    id: "saveUser",
                                    description: err.reason || t(err.error),
                                });

                                return;
                            }

                            history.push(`/edit/user/${res._id}`);

                            toaster.success(t("User data saved"), {
                                id: "saveUser",
                            });
                        },
                    );
                }}
                onError={form => console.error("Cannot save user", form)}
            >
                {submitButton}
            </Form>
        </>
    );
};

export default UserForm;
