import React from "react";
import { Spinner, Pane } from "evergreen-ui";

const Loader = () => (
    <Pane display="flex" justifyContent="center">
        <Spinner delay={100} />
    </Pane>
);

export default Loader;
