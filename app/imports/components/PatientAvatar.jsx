import React from "react";
import get from "lodash/get";
import { Avatar, majorScale } from "evergreen-ui";

const PatientAvatar = ({ patient, ...props }) => (
    <Avatar
        sizeLimitOneCharacter={majorScale(1)}
        name={`${get(patient, "firstName") || ""} ${get(patient, "lastName") ||
            ""}`}
        {...props}
    />
);

export default PatientAvatar;
