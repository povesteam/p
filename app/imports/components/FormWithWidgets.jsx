import React from "react";
import Form from "react-jsonschema-form";
import TextareaAutosize from "react-textarea-autosize";

// https://react-jsonschema-form.readthedocs.io/en/latest/advanced-customization/#customizing-the-default-fields-and-widgets
const TextareaWidget = ({
    value,
    readonly,
    autofocus,
    onChange,
    formContext,
    rawErrors,
    ...props
}) => (
    <TextareaAutosize
        readOnly={readonly}
        autoFocus={autofocus}
        minRows={2}
        className="form-control"
        defaultValue={value}
        onChange={ref => onChange(ref.target.value)}
        {...props}
    />
);

const FormWithWidgets = props => (
    <Form widgets={{ TextareaWidget }} {...props} />
);

export default FormWithWidgets;
