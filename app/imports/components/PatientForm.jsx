import Loader from "./Loader";
import Form from "./FormWithWidgets";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";
import React, { useState, useEffect } from "react";
import { PatientHeaderWidget } from "./PatientHeader";
import ReadOnlySchemaField from "./ReadOnlySchemaField";
import { Heading, Button, Pane, majorScale, toaster } from "evergreen-ui";

// https://react-jsonschema-form.readthedocs.io/en/latest/form-customization/

const PatientForm = ({ loading, data, readOnly }) => {
    const { t } = useTranslation();
    const history = useHistory();

    const [saving, setSaving] = useState(false);
    const [formData, setFormData] = useState();

    useEffect(() => {
        if (data) {
            setFormData(data);
        }
    }, [data]);

    if (loading) {
        return <Loader />;
    }

    const schema = {
        type: "object",
        // required: ["firstName", "lastName"],
        properties: {
            firstName: {
                type: "string",
                title: t("First name"),
            },
            lastName: {
                type: "string",
                title: t("Last name"),
            },
            birthDate: {
                type: "string",
                format: "date",
                title: t("Birth date"),
            },
            gender: {
                title: t("Gender"),
                type: "string",
                enum: ["m", "f"],
                enumNames: ["M", "F"],
            },
            idNumber: {
                title: t("Identification number"),
                type: "string",
            },
            idDocument: {
                title: t("Identification document"),
                type: "string",
            },
            countryCode: {
                title: t("Country"),
                type: "string",
                default: "ro",
                enum: ["ro", "bd"],
                enumNames: ["România", "Bangladesh"],
            },
            region: {
                title: t("State/Province/Region"),
                type: "string",
            },
            city: {
                title: t("City"),
                type: "string",
            },
            address: {
                title: t("Address"),
                type: "string",
            },
            postCode: {
                title: t("Post code"),
                type: "string",
            },
            phone: {
                title: t("Phone"),
                type: "string",
                minLength: 3,
            },
            email: {
                title: t("Email"),
                type: "string",
                format: "email",
            },
            otherInfo: {
                title: t("Other information"),
                type: "string",
            },
        },
    };

    const uiSchema = {
        otherInfo: {
            "ui:widget": "textarea",
        },
    };

    const readOnlyOptions = {};
    let headings;
    let submitButton;

    if (readOnly) {
        headings = (
            <Pane display="flex" marginBottom={majorScale(2)}>
                <Pane flex={1}>
                    <PatientHeaderWidget patient={data} />
                </Pane>
                <Pane>
                    <Button
                        iconBefore="series-search"
                        onClick={() => {
                            history.push(`/list/investigation/${data._id}`);
                        }}
                        marginRight={majorScale(1)}
                    >
                        {t("Investigations")}
                    </Button>
                    <Button
                        iconBefore="edit"
                        onClick={() =>
                            history.push(`/edit/patient/${data._id}`)
                        }
                    >
                        {t("Edit")}
                    </Button>
                </Pane>
            </Pane>
        );

        readOnlyOptions.fields = {
            SchemaField: ReadOnlySchemaField,
        };

        submitButton = <span />;
    } else {
        headings = (
            <>
                <Heading size={700} paddingBottom={majorScale(2)}>
                    {t("New patient")}
                </Heading>

                <Heading size={500} paddingBottom={majorScale(2)}>
                    {t("Add a patient to the system")}
                </Heading>
            </>
        );

        submitButton = (
            <Pane
                background="tint2"
                display="flex"
                flexDirection="row-reverse"
                position="sticky"
                bottom="0"
                borderTop
                paddingTop={majorScale(2)}
                paddingBottom={majorScale(2)}
            >
                <Pane>
                    <Button
                        type="submit"
                        appearance="primary"
                        iconBefore={saving ? null : "floppy-disk"}
                        isLoading={saving}
                    >
                        {t("Save")}
                    </Button>
                </Pane>
                <Pane flex={1}>
                    <Button
                        appearance="minimal"
                        iconBefore="arrow-left"
                        onClick={() => history.push("/list/patient")}
                    >
                        {t("Back")}
                    </Button>
                </Pane>
            </Pane>
        );
    }

    return (
        <>
            {headings}

            <Form
                disabled={saving}
                schema={schema}
                uiSchema={uiSchema}
                formData={formData}
                {...readOnlyOptions}
                onSubmit={({ formData }) => {
                    setSaving(true);
                    setFormData(formData);

                    Meteor.call(
                        "savePatient",
                        {
                            doc: formData,
                        },
                        (err, res) => {
                            setSaving(false);

                            if (err) {
                                console.error(
                                    "Error while saving patient",
                                    err,
                                );

                                toaster.danger(t("Cannot save patient data"), {
                                    id: "savePatient",
                                    description: err.reason || t(err.error),
                                });

                                return;
                            }

                            history.push(`/view/patient/${res._id}`);

                            toaster.success(t("Patient data saved"), {
                                id: "savePatient",
                            });
                        },
                    );
                }}
                onError={form => console.error("Cannot save patient", form)}
            >
                {submitButton}
            </Form>
        </>
    );
};

export default PatientForm;
