import React from "react";
import UserForm from "./UserForm";
import compose from "lodash/fp/compose";
import subscribe from "/imports/api/subscribe";
import { withTracker } from "meteor/react-meteor-data";
import withRouterParams from "/imports/hocs/withRouterParams";

const UserFormEdit = compose([
    withRouterParams(),
    withTracker(({ userId }) => {
        const userHandle = subscribe("user", { userId });
        const loading = !userHandle.ready();

        return {
            loading,
            user: Meteor.users.findOne({ _id: userId }),
        };
    }),
])(({ user, ...props }) => <UserForm user={user} {...props} />);

export default UserFormEdit;
