import React from "react";
import indexOf from "lodash/indexOf";
import { Card, Paragraph, Text, Strong } from "evergreen-ui";
import SchemaField from "react-jsonschema-form/lib/components/fields/SchemaField";

const ReadOnlySchemaField = function(props) {
    if (props.schema.type === "object") {
        return (
            <Card
                background="yellowTint"
                padding={24}
                marginBottom={16}
                elevation={1}
            >
                <SchemaField {...props} />
            </Card>
        );
    }

    if (!props.formData) {
        // hide fields without value
        return null;
    }

    let displayValue = props.formData;

    if (props.schema.enum) {
        // get display value for enum field
        const index = indexOf(props.schema.enum, props.formData);
        displayValue = props.schema.enumNames[index] || props.formData;
    }

    return (
        <Paragraph>
            <Text color="muted">{props.schema.title}</Text>
            &nbsp;&nbsp;&nbsp;
            <Strong>{displayValue}</Strong>
        </Paragraph>
    );
};

export default ReadOnlySchemaField;
