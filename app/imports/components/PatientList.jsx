import subscribe from "/imports/api/subscribe";
import Patients from "/imports/models/patients";
import PatientListTable from "./PatientListTable.jsx";
import { withTracker } from "meteor/react-meteor-data";

export default withTracker(() => {
    const patientsHandle = subscribe("patients");
    const loading = !patientsHandle.ready();

    const patients = Patients.find(
        {},
        {
            sort: {
                createdAt: -1,
            },
        },
    ).fetch();

    return {
        loading,
        patients,
    };
})(PatientListTable);
