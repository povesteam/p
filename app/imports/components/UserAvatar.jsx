import React from "react";
import get from "lodash/get";
import { withTracker } from "meteor/react-meteor-data";
import { Avatar, Text, Pane, majorScale, Heading } from "evergreen-ui";

const UserAvatar = ({ user, withName, big, ...props }) => {
    const name =
        get(user, "profile.name") ||
        get(user, "username") ||
        get(user, "_id") ||
        "";

    const avatar = (
        <Avatar
            sizeLimitOneCharacter={majorScale(1)}
            name={name}
            size={majorScale(big ? 4 : 3)}
            {...props}
        />
    );

    const displayName = withName ? (
        big ? (
            <Heading size={700}>{name}</Heading>
        ) : (
            <Text>{name}</Text>
        )
    ) : null;

    if (!displayName) {
        return avatar;
    }

    return (
        <Pane display="flex">
            {avatar}
            <Text marginLeft={majorScale(1)} size={300} fontWeight={500}>
                {displayName}
            </Text>
        </Pane>
    );
};

export const MyUserAvatar = withTracker(() => ({
    user: Meteor.user(),
}))(({ user }) => <UserAvatar user={user} />);

export default UserAvatar;
