import apply from "./apply";

const publish = ({ name, always, role, checks = {} }, callback) => {
    Meteor.publish(always ? null : name, (params = {}) => {
        return apply({
            type: "publish",
            name,
            role,
            checks,
            params,
            callback,
        });
    });
};

export default publish;
