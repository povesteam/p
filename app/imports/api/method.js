import apply from "./apply";

const method = ({ name, role, checks = {} }, callback) => {
    Meteor.methods({
        [name]: (params = {}) => {
            return apply({
                type: "method",
                name,
                role,
                checks,
                params,
                callback,
            });
        },
    });
};

export default method;
