/* eslint no-underscore-dangle: 0 */

const simulateLag = () => {
    if (Meteor.isDevelopment) {
        Meteor._sleepForMs(1000);
    }
};

export default simulateLag;
