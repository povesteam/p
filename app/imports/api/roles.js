import includes from "lodash/includes";

export const userHasRole = (user, role) => {
    return role && includes(user.roles, role);
};
