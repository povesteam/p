import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

i18n.use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translations: {
                    ERROR_NO_INFORMATION: "No information received",
                    ERROR_DUPLICATE_USERNAME: "Username already exists",
                    ERROR_ACCESS_DENIED: "Access denied",
                },
            },
            ro: {
                translations: {
                    Home: "Acasă",
                    Search: "Caută",
                    Edit: "Modifică",
                    Delete: "Șterge",
                    Save: "Salvează",
                    Back: "Înapoi",

                    Users: "Utilizatori",
                    "New user": "Utilizator nou",
                    "User data saved":
                        "Datele despre utilizator au fost salvate",
                    "Cannot save user data":
                        "Nu se pot salva datele despre utilizator",
                    Password: "Parolă",
                    "Leave empty to keep current password":
                        "Lasă necompletat pentru a păstra parola actuală",
                    Role: "Rol",
                    Receptionist: "Recepționist",
                    Medic: "Medic",
                    Administrator: "Administrator",
                    "All medics": "Toți medicii",
                    "Investigation types": "Tipuri de investigații",
                    Checkup: "Consultație",

                    Patients: "Pacienți",
                    "New patient": "Pacient nou",
                    "Add a patient to the system": "Adaugă pacient în sistem",
                    "Patient data saved":
                        "Datele despre pacient au fost salvate",
                    "Cannot save patient data":
                        "Nu se pot salva datele despre pacient",

                    "First name": "Prenume",
                    "Last name": "Nume de familie",
                    "Birth date": "Data nașterii",
                    "Identification number": "CNP",
                    "Identification document": "Act de identitate",
                    Country: "Țară",
                    "State/Province/Region": "Județ",
                    City: "Oraș",
                    Address: "Adresă",
                    "Post code": "Cod poștal",
                    Phone: "Telefon",
                    "Other information": "Alte informații",

                    Investigations: "Investigații",
                    "New investigation": "Investigație nouă",
                    "Investigation details": "Detalii investigație",
                    "Medical history": "Antecedente personale patologice",
                    Allergies: "Alergii",
                    "Physical examination": "Examen clinic",
                    Diagnosis: "Diagnostic",
                    Recommendations: "Recomandări",
                    "Medical leave": "Concediu medical",

                    ERROR_NO_INFORMATION: "Nicio informație primită",
                    ERROR_DUPLICATE_USERNAME: "Utilizatorul există deja",
                    ERROR_ACCESS_DENIED: "Acces interzis",
                },
            },
        },
        fallbackLng: "en",
        debug: true,

        ns: ["translations"],
        defaultNS: "translations",

        // we use content as keys
        keySeparator: false,

        interpolation: {
            escapeValue: false,
        },
    });

export default i18n;
