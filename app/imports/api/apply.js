import get from "lodash/get";
import { userHasRole } from "./roles";
import simulateLag from "/imports/api/simulateLag";

const apply = ({ type, name, role, checks, params, callback }) => {
    const user = Meteor.user();
    const usernameDisplay = get(user, "username") || get(user, "_id") || "--";

    // check if user has the required role
    if (role !== "*" && !userHasRole(user, role)) {
        console.error(
            "Access denied for",
            role,
            usernameDisplay,
            "to",
            type,
            name,
            params,
        );

        simulateLag();

        throw new Meteor.Error("ERROR_ACCESS_DENIED");
    }

    console.log(type, "for", role, usernameDisplay, name, params);

    simulateLag();

    check(params, checks);

    return callback(params);
};

export default apply;
