const subscribe = (name, params = {}) =>
    Meteor.subscribe(name, params, {
        onStop: err => err && console.error(`Cannot subscribe to ${name}`, err),
    });

export default subscribe;
