import React, { useRef, useEffect } from "react";
import { Template } from "meteor/templating";
import { Blaze } from "meteor/blaze";
import styled, { css } from "styled-components";

const Elements = styled.div`
    ${({ inline }) =>
        inline
            ? css`
                  position: relative;
                  top: 8px;
              `
            : css`
                  background: wheat;
                  display: flex;
                  align-items: center;
                  justify-content: center;
                  height: 100%;
              `}

    .login-link-text,
    .login-close-text,
    .additional-link {
        cursor: pointer;
    }

    .accounts-dialog {
        ${({ inline }) =>
            inline
                ? css`
                      position: absolute;
                      top: -10px;
                      right: 0px;
                  `
                : css`
                      position: relative;
                      top: -20px;
                  `}

        background: white;
        z-index: 999;
        padding: 10px;
        border: 2px solid #237cd7;
        border-radius: 3px;
        box-shadow: 2px 2px 1vmax 0px rgba(0, 0, 0, 0.4);
    }

    .loading {
        position: absolute;
        top: 10px;
        right: 10px;
        &::after {
            content: "☁️";
            color: #237cd7;
        }
    }

    .login-form {
        label {
            display: block;
            color: #237cd7;
        }

        input {
            border: 2px solid #237cd7;
            padding: 3px;
            border-radius: 3px;
            margin-bottom: 0.5em;
        }
    }

    .error-message {
        color: #d74d23;
        max-width: 156px;
        font-size: 0.7em;
        line-height: 1.2em;
        margin-bottom: 10px;
    }

    .login-button {
        background: #237cd7;
        color: white;

        font-size: 1em;
        padding: 0.25em 1em;
        border: 2px solid #237cd7;
        border-radius: 3px;
        text-align: center;
        margin-bottom: 0.5em;
        width: 100%;
    }

    .additional-link,
    .login-close-text {
        color: #237cd7;
        text-decoration: underline;
    }

    .login-close-text-clear {
        margin-bottom: 10px;
    }
`;

const AccountsUIWrapper = props => {
    const container = useRef(null);

    useEffect(() => {
        const view = Blaze.renderWithData(
            Template.loginButtons,
            props.inline ? { align: "right" } : {},
            container.current,
        );
        return () => Blaze.remove(view);
    });

    return (
        <Elements {...props}>
            <div ref={container} />
        </Elements>
    );
};

export default AccountsUIWrapper;
