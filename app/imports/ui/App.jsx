import React from "react";
import Gate from "/imports/components/Gate";
import Home from "/imports/components/Home";
import { useTranslation } from "react-i18next";
import AccountsUIWrapper from "./AccountsUIWrapper";
import UserList from "/imports/components/UserList";
import UserForm from "/imports/components/UserForm";
import { withTracker } from "meteor/react-meteor-data";
import { Pane, Button, majorScale } from "evergreen-ui";
import PatientForm from "/imports/components/PatientForm";
import PatientList from "/imports/components/PatientList";
import UserFormEdit from "/imports/components/UserFormEdit";
import { Switch, Route, useHistory } from "react-router-dom";
import { MyUserAvatar } from "/imports/components/UserAvatar";
import PatientFormEdit from "/imports/components/PatientFormEdit";
import ConnectionStatus from "/imports/components/ConnectionStatus";
import InvestigationPage from "/imports/components/InvestigationPage";
import InvestigationPrintPage from "/imports/components/InvestigationPrintPage";

const App = ({ user }) => {
    const { t, i18n } = useTranslation();

    const history = useHistory();

    if (!user) {
        return (
            <>
                <ConnectionStatus />

                <AccountsUIWrapper />
            </>
        );
    }

    return (
        <>
            <Switch>
                <Route path="/print" />
                <Route>
                    <ConnectionStatus />

                    <button onClick={() => i18n.changeLanguage("ro")}>
                        ro
                    </button>
                    <button onClick={() => i18n.changeLanguage("en")}>
                        en
                    </button>

                    <Pane
                        display="flex"
                        padding={16}
                        background="tint2"
                        borderRadius={3}
                    >
                        <Pane flex={1} display="flex">
                            <Button
                                appearance="minimal"
                                iconBefore="home"
                                onClick={() => history.push(`/`)}
                            >
                                {t("Home")}
                            </Button>

                            <Gate role="medic">
                                <Button
                                    appearance="minimal"
                                    iconBefore="people"
                                    onClick={() =>
                                        history.push(`/list/patient`)
                                    }
                                >
                                    {t("Patients")}
                                </Button>

                                <Button
                                    appearance="minimal"
                                    iconBefore="plus"
                                    onClick={() => history.push(`/new/patient`)}
                                >
                                    {t("New patient")}
                                </Button>
                            </Gate>

                            <Gate role="admin">
                                <Button
                                    appearance="minimal"
                                    iconBefore="badge"
                                    onClick={() => history.push(`/list/user`)}
                                >
                                    {t("Users")}
                                </Button>

                                <Button
                                    appearance="minimal"
                                    iconBefore="plus"
                                    onClick={() => history.push(`/new/user`)}
                                >
                                    {t("New user")}
                                </Button>
                            </Gate>
                        </Pane>
                        <Pane marginRight={majorScale(1)}>
                            <MyUserAvatar />
                        </Pane>
                        <Pane>
                            <AccountsUIWrapper inline />
                        </Pane>
                    </Pane>
                </Route>
            </Switch>

            <Route exact path="/">
                <Home />
            </Route>

            <Gate role="medic">
                <Route path="/list/patient">
                    <PatientList />
                </Route>

                <Route path="/new/patient">
                    <Pane
                        padding={16}
                        background="tint2"
                        borderRadius={3}
                        maxWidth={500}
                        marginX="auto"
                    >
                        <PatientForm />
                    </Pane>
                </Route>

                <Route path="/view/patient/:patientId">
                    <Pane
                        padding={16}
                        borderRadius={3}
                        maxWidth={500}
                        marginX="auto"
                    >
                        <PatientFormEdit readOnly />
                    </Pane>
                </Route>

                <Route path="/edit/patient/:patientId">
                    <Pane
                        padding={16}
                        background="tint2"
                        borderRadius={3}
                        maxWidth={500}
                        marginX="auto"
                    >
                        <PatientFormEdit />
                    </Pane>
                </Route>

                <Route path="/list/investigation/:patientId/:investigationId?">
                    <Pane padding={16} borderRadius={3}>
                        <InvestigationPage />
                    </Pane>
                </Route>

                <Route path="/print/investigation/:patientId/:investigationId">
                    <InvestigationPrintPage />
                </Route>
            </Gate>

            <Gate role="admin">
                <Route path="/list/user">
                    <UserList />
                </Route>

                <Route path="/new/user">
                    <Pane
                        padding={16}
                        background="tint2"
                        borderRadius={3}
                        maxWidth={500}
                        marginX="auto"
                    >
                        <UserForm user={{ profile: {} }} />
                    </Pane>
                </Route>

                <Route path="/edit/user/:userId">
                    <Pane
                        padding={16}
                        background="tint2"
                        borderRadius={3}
                        maxWidth={500}
                        marginX="auto"
                    >
                        <UserFormEdit />
                    </Pane>
                </Route>
            </Gate>
        </>
    );
};

export default withTracker(() => ({
    user: Meteor.user(),
}))(App);
