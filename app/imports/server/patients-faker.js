import faker from "faker";
import each from "lodash/each";
import range from "lodash/range";
import random from "lodash/random";
import sample from "lodash/sample";
import Patients from "/imports/models/patients";
import Investigations from "/imports/models/investigations";

Meteor.startup(() => {
    Patients.remove({
        fake: true,
    });

    Investigations.remove({
        fake: true,
    });

    const medicIds = Meteor.users
        .find({ roles: "medic" })
        .fetch()
        .map(u => u._id);

    each(range(15), i => {
        const patientId = Patients.insert({
            fake: true,
            createdAt: faker.date.past(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            gender: faker.random.arrayElement([null, "m", "f"]),
            idNumber: faker.finance.account(),
            email: faker.internet.email(),
        });

        each(range(random(1, 5)), i => {
            Investigations.insert({
                fake: true,
                patientId,
                date: sample([
                    faker.date.past(),
                    faker.date.future(),
                    faker.date.recent(),
                ]),

                medicId: sample(medicIds),
                symptoms: faker.lorem.sentence(),
                medicalHistory: faker.lorem.sentences(),
                allergies: faker.lorem.words(),
                physicalExamination: faker.lorem.lines(),
                diagnosis: faker.lorem.text(),
                recommendations: faker.lorem.paragraphs(),
                medicalLeave: faker.random.boolean(),
                medicalLeaveNumber: faker.finance.bic(),
                otherInfo: faker.lorem.paragraphs(),

                createdAt: sample([
                    faker.date.past(),
                    faker.date.future(),
                    faker.date.recent(),
                ]),
            });
        });
    });
});
