import get from "lodash/get";
import some from "lodash/some";
import method from "/imports/api/method";
import publish from "/imports/api/publish";

const relevantFields = ["profile.firstName", "profile.lastName", "roles"];

const publishFields = {
    createdAt: 1,
    username: 1,
    profile: 1,
    roles: 1,
    investigationTypes: 1,
};

method(
    {
        name: "saveUser",
        role: "admin",
        checks: {
            doc: Object,
        },
    },
    ({ doc }) => {
        const { _id, password, ...fields } = doc;

        if (!fields.username) {
            throw new Meteor.Error("ERROR_NO_INFORMATION");
        }

        if (
            Meteor.users.findOne({
                username: fields.username,
                _id: { $ne: _id || "" },
            })
        ) {
            throw new Meteor.Error("ERROR_DUPLICATE_USERNAME");
        }

        if (!some(relevantFields, k => get(fields, k))) {
            throw new Meteor.Error("ERROR_NO_INFORMATION");
        }

        fields.profile.name = `${fields.profile.firstName || ""} ${fields
            .profile.lastName || ""}`.trim();

        let userId;

        // todo security
        if (_id) {
            Meteor.users.update(
                {
                    _id,
                },
                {
                    $set: {
                        ...fields,
                        updatedAt: new Date(),
                    },
                },
            );

            userId = _id;
        } else {
            userId = Meteor.users.insert({
                ...fields,
                createdAt: new Date(),
            });
        }

        if (password) {
            Accounts.setPassword(userId, password);
        }

        return { _id: userId };
    },
);

publish(
    {
        name: "users",
        role: "admin",
    },
    () => Meteor.users.find({}, { fields: publishFields }),
);

publish(
    {
        name: "user",
        role: "admin",
        checks: {
            userId: String,
        },
    },
    ({ userId }) =>
        Meteor.users.find({ _id: userId }, { fields: publishFields }),
);

publish(
    {
        name: "medics",
        role: "*",
    },
    () => Meteor.users.find({ roles: "medic" }, { fields: publishFields }),
);

// always publish my details
publish(
    {
        name: "myDetails",
        always: true,
        role: "*",
    },
    () =>
        Meteor.users.find({ _id: Meteor.userId() }, { fields: publishFields }),
);
