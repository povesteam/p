import some from "lodash/some";
import { Meteor } from "meteor/meteor";
import method from "/imports/api/method";
import publish from "/imports/api/publish";
import Patients from "/imports/models/patients";

import "./patients-faker.js";

const relevantFields = [
    "firstName",
    "lastName",
    "gender",
    "birthDate",
    "idNumber",
    "idDocument",
    "phone",
    "email",
    "otherInfo",
];

method(
    {
        name: "savePatient",
        role: "medic",
        checks: {
            doc: Object,
        },
    },
    ({ doc }) => {
        const { _id, ...fields } = doc;

        if (!some(relevantFields, k => fields[k])) {
            throw new Meteor.Error("ERROR_NO_INFORMATION");
        }

        // todo security
        if (_id) {
            Patients.update(
                {
                    _id,
                },
                {
                    $set: {
                        ...fields,
                        updatedAt: new Date(),
                    },
                },
            );

            return { _id };
        }

        const insertedId = Patients.insert({
            ...fields,
            createdAt: new Date(),
        });

        return { _id: insertedId };
    },
);

publish(
    {
        name: "patients",
        role: "medic",
    },
    () => Patients.find(),
);

publish(
    {
        name: "patientsForReceptionist",
        role: "receptionist",
    },
    () => Patients.find(),
);

publish(
    {
        name: "patient",
        role: "medic",
        checks: {
            patientId: String,
        },
    },
    ({ patientId }) => Patients.find({ _id: patientId }),
);
