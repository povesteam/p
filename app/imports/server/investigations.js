import moment from "moment";
import some from "lodash/some";
import { Meteor } from "meteor/meteor";
import method from "/imports/api/method";
import publish from "/imports/api/publish";
import Investigations from "/imports/models/investigations";

const relevantFields = ["medicId", "otherInfo"];

method(
    {
        name: "saveInvestigation",
        role: "medic",
        checks: {
            doc: Object,
        },
    },
    ({ doc }) => {
        const { _id, date, ...fields } = doc;

        if (!some(relevantFields, k => fields[k])) {
            throw new Meteor.Error("ERROR_NO_INFORMATION");
        }

        if (!date) {
            throw new Meteor.Error("ERROR_NO_INFORMATION");
        }

        fields.date = moment(date).toDate();

        // todo security
        if (_id) {
            Investigations.update(
                {
                    _id,
                },
                {
                    $set: {
                        ...fields,
                        updatedAt: new Date(),
                    },
                },
            );

            return { _id };
        }

        const insertedId = Investigations.insert({
            ...fields,
            createdAt: new Date(),
        });

        return { _id: insertedId };
    },
);

publish(
    {
        name: "investigations",
        role: "medic",
        checks: {
            patientId: String,
        },
    },
    ({ patientId }) => Investigations.find({ patientId }),
);

publish(
    {
        name: "investigationsForReceptionist",
        role: "receptionist",
    },
    () => Investigations.find(),
);

publish(
    {
        name: "investigation",
        role: "medic",
        checks: {
            investigationId: String,
        },
    },
    ({ investigationId }) => Investigations.find({ _id: investigationId }),
);
