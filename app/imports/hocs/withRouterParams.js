import React from "react";
import { withRouter } from "react-router";

const withRouterParams = () => WrappedComponent => {
    const HasRouterParams = ({
        match,
        location,
        history,
        ...originalProps
    }) => {
        return <WrappedComponent {...match.params} {...originalProps} />;
    };

    return withRouter(HasRouterParams);
};

export default withRouterParams;
