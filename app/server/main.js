import "/imports/api/roles.js";
import "/imports/server/users.js";
import "/imports/server/patients.js";
import "/imports/accounts/server.js";
import "/imports/server/investigations.js";

console.log("Server starting...");
Meteor.startup(() => {
    console.log("Server started ");
});
